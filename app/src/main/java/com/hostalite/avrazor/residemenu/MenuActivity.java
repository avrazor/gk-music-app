package com.hostalite.avrazor.residemenu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.config.CaocConfig;

public class MenuActivity extends FragmentActivity implements View.OnClickListener{

    private ResideMenu resideMenu;
    private MenuActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemProfile;
    private ResideMenuItem itemCalendar;
    private ResideMenuItem itemSettings;
    //private ResideMenuItem itemContact;

    private ResideMenuItem itemFacebook;
    private ResideMenuItem itemTwitter;
    private ResideMenuItem itemInstagram;
    private ResideMenuItem itemYoutube;
    //private ResideMenuItem itemRevebnation;

    private static final String TAG = "Crashing";
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mContext = this;
        setUpMenu();
        if( savedInstanceState == null )
            changeFragment(new HomeFragment());

        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //default: CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM
                .enabled(false) //default: true
                .showErrorDetails(false) //default: true
                .showRestartButton(false) //default: true
                .logErrorOnRestart(false) //default: true
                .trackActivities(true) //default: false
                .minTimeBetweenCrashesMs(2000) //default: 3000
                .errorDrawable(R.drawable.customactivityoncrash_error_image) //default: bug image
                .restartActivity(MenuActivity.class) //default: null (your app's launch activity)
                .errorActivity(CustomErrorActivity.class) //default: null (default error activity)
                .eventListener(new CustomEventListener()) //default: null
                .apply();
    }

    private static class CustomEventListener implements CustomActivityOnCrash.EventListener {
        @Override
        public void onLaunchErrorActivity() {
            Log.i(TAG, "onLaunchErrorActivity()");
        }

        @Override
        public void onRestartAppFromErrorActivity() {
            Log.i(TAG, "onRestartAppFromErrorActivity()");
        }

        @Override
        public void onCloseAppFromErrorActivity() {
            Log.i(TAG, "onCloseAppFromErrorActivity()");
        }
    }

    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        //resideMenu.setUse3D(true);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
        resideMenu.setScaleValue(0.6f);

        // create menu items;
        itemHome     = new ResideMenuItem(this, R.drawable.play,     "Home");
        itemProfile  = new ResideMenuItem(this, R.drawable.music,  "Music Playlist");
        itemCalendar = new ResideMenuItem(this, R.drawable.video, "Video Wall");
        itemSettings = new ResideMenuItem(this, R.drawable.contact, "Contact");
        //itemContact = new ResideMenuItem(this, R.drawable.contact, "Contact");

        itemFacebook     = new ResideMenuItem(this, R.drawable.facebook,     "Facebook");
        itemTwitter  = new ResideMenuItem(this, R.drawable.twitter,  "Twitter");
        itemInstagram = new ResideMenuItem(this, R.drawable.instagram, "Instagram");
        itemYoutube = new ResideMenuItem(this, R.drawable.youtube, "Youtube");
        //itemRevebnation = new ResideMenuItem(this, R.drawable.revebnation, "Revebnation");

        itemHome.setOnClickListener(this);
        itemProfile.setOnClickListener(this);
        itemCalendar.setOnClickListener(this);
        itemSettings.setOnClickListener(this);

        itemFacebook.setOnClickListener(this);
        itemTwitter.setOnClickListener(this);
        itemInstagram.setOnClickListener(this);
        itemYoutube.setOnClickListener(this);
        //itemRevebnation.setOnClickListener(this);

        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemCalendar, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);
        //resideMenu.addMenuItem(itemContact, ResideMenu.DIRECTION_LEFT);

        resideMenu.addMenuItem(itemFacebook, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemTwitter, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemInstagram, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemYoutube, ResideMenu.DIRECTION_RIGHT);
        //resideMenu.addMenuItem(itemRevebnation, ResideMenu.DIRECTION_RIGHT);

        // You can disable a direction by setting ->
        // resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        findViewById(R.id.title_bar_right_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });
    }

    public static String FACEBOOK_URL = "https://www.facebook.com/gkmusicg";
    public static String FACEBOOK_PAGE_ID = "364036420783489";

    //method to get the right URL to use in the intent
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;

            boolean activated =  packageManager.getApplicationInfo("com.facebook.katana", 0).enabled;
            if(activated){
                if ((versionCode >= 3002850)) {
                    return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
                } else {
                    return "fb://page/" + FACEBOOK_PAGE_ID;
                }
            }else{
                return FACEBOOK_URL;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemHome){
            changeFragment(new HomeFragment());
        }else if (view == itemProfile){
            changeFragment(new ProfileFragment());
        }else if (view == itemCalendar){
            changeFragment(new CalendarFragment());
        }else if (view == itemSettings){
            changeFragment(new SettingsFragment());
        }else if (view == itemFacebook){
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/gkmusicg")));
            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
            String facebookUrl = getFacebookPageURL(this);
            facebookIntent.setData(Uri.parse(facebookUrl));
            startActivity(facebookIntent);
        }else if (view == itemTwitter){
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.twitter.com/gkmusicgk")));
            String twitter_user_name = "gkmusicgk";
            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + twitter_user_name)));
            }catch (Exception e){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + twitter_user_name)));
            }
        }else if (view == itemInstagram){
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/@gkmusicgk")));
            Uri insta = Uri.parse("https://instagram.com/gabrielk_sings");
            Intent Instagram = new Intent(Intent.ACTION_VIEW, insta);
            Instagram.setPackage("com.instagram.android");
            try{
                startActivity(Instagram);
            }catch(ActivityNotFoundException e){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://instagram.com/gabrielk_sings")));
            }
        }else if (view == itemYoutube){
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/c/Gabrielk")));
            Uri youtu = Uri.parse("https://www.youtube.com/c/Gabrielk");
            Intent Youtube = new Intent(Intent.ACTION_VIEW, youtu);
            Youtube.setPackage("com.google.android.youtube");
            try{
                startActivity(Youtube);
            }catch(ActivityNotFoundException e){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/c/gabrielk")));
            }
        }

        resideMenu.closeMenu();
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            //Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            //Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu(){
        return resideMenu;
    }


}
