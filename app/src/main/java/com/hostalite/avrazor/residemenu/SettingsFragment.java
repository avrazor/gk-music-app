package com.hostalite.avrazor.residemenu;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import im.delight.android.webview.AdvancedWebView;


public class SettingsFragment extends Fragment {

    private AdvancedWebView mWebView;
    private final String url= "https://gabrielkmusic.com/?page_id=102";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings, container, false);

        mWebView = rootView.findViewById(R.id.webview);
        //mWebView.setListener(getActivity(), (AdvancedWebView.Listener) this);
        mWebView.setGeolocationEnabled(false);
        mWebView.setMixedContentAllowed(true);
        mWebView.setCookiesEnabled(true);
        mWebView.setThirdPartyCookiesEnabled(true);


        /** mWebView.setWebViewClient(new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
        Toast.makeText(getActivity(), "Finished loading", Toast.LENGTH_SHORT).show();
        }
        });  **/

        /** mWebView.setWebChromeClient(new WebChromeClient() {

        @Override
        public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
        Toast.makeText(getActivity(), title, Toast.LENGTH_SHORT).show();
        }

        }); **/
        //mWebView.addHttpHeader("X-Requested-With", "");
        //mWebView.loadUrl(url);

        new MyAsynTask().execute();

        return rootView;
    }

    private class MyAsynTask extends AsyncTask<Void, Void, Document> {
        @Override
        protected void onPreExecute() {
            mWebView.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon){

                }
            });

            super.onPreExecute();
        }

        @Override
        protected Document doInBackground(Void... voids) {

            Document document = null;
            try {
                document= Jsoup.connect(url).get();
                document.getElementById("lc_page_header").remove();
                document.getElementById("footer_sidebars").remove();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            super.onPostExecute(document);
            mWebView.loadDataWithBaseURL(url,document.toString(),"text/html","utf-8","");

            mWebView.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    mWebView.addHttpHeader("X-Requested-With", "");
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, request);
                }

                @SuppressWarnings("deprecation")
                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    mWebView.loadUrl("file:///assets/index.html");
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr, String failingUrl) {
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());

                }
            });
        }
    }




    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }


}
